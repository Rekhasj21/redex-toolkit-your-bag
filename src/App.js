import Navbar from "./components/Navbar";
import CartContainer from "./components/CartContainer";
import { useSelector, useDispatch } from 'react-redux'
import { useEffect } from 'react'
import { calculateTotal, getCartItems } from './Features/Cart/CartSlice'
import Modal from "./components/Modal"

function App() {
  const { cartItems, isLoading } = useSelector(store => store.cart)
  const { isOpen } = useSelector(store => store.modal)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getCartItems('random'))
  }, [])

  useEffect(() => {
    dispatch(calculateTotal())
  }, [cartItems])

  if (isLoading) {
    return (
      <div className="loading">
        <h1>Loading...</h1>
      </div>
    )
  }

  return (
    <main>
      {isOpen && <Modal />}
      <Navbar />
      <CartContainer />
    </main>
  )
}
export default App;
